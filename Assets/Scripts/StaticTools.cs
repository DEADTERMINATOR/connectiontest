﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticTools
{
    /// <summary>
    /// Compares two float values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first float.</param>
    /// <param name="b">The second float.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(float a, float b, float threshold)
    {
        return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
    }

    /// <summary>
    /// Compares two Vecter2 values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first Vecter2.</param>
    /// <param name="b">The second Vecter2.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(Vector2 a, Vector2 b, float threshold)
    {
        return (ThresholdApproximately(a.x, b.x, threshold) && ThresholdApproximately(a.y, b.y, threshold));
    }

    /// <summary>
    /// Compares two Vecter2 values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first Vecter2.</param>
    /// <param name="b">The second Vecter2.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(Vector3 a, Vector3 b, float threshold)
    {
        return (ThresholdApproximately(a.x, b.x, threshold) && ThresholdApproximately(a.y, b.y, threshold) && ThresholdApproximately(a.z, b.z, threshold));
    }
}

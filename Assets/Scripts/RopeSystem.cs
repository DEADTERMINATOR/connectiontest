﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RopeSystem : MonoBehaviour
{
    public struct RopePosition
    {
        public enum AssociatedPlayer { One, Two };

        public Vector2 Position;
        public Vector2 ForceDirection;
        public AssociatedPlayer OwningPlayer;
        public bool IsPlayer;
    }

    public Player PlayerOne;
    public Player PlayerTwo;
    public LineRenderer RopeRenderer;
    public LayerMask RopeLayerMask;

    private float _maxRopeDistance = 18f;
    
    private LinkedList<RopePosition> _ropePositions = new LinkedList<RopePosition>();
    private LinkedListNode<RopePosition> _lastPlayerOneRopePosition;
    private LinkedListNode<RopePosition> _lastPlayerTwoRopePosition;

    private int _totalPointCount = 0;


    private void Start()
    {
        RopeRenderer.positionCount = 2;
        RopeRenderer.SetPosition(1, PlayerTwo.transform.position);
        RopeRenderer.SetPosition(0, PlayerOne.transform.position);

        _ropePositions.AddFirst(new RopePosition { Position = PlayerOne.transform.position, ForceDirection = Vector2.zero, OwningPlayer = RopePosition.AssociatedPlayer.Two, IsPlayer = true });
        _ropePositions.AddLast(new RopePosition { Position = PlayerTwo.transform.position, ForceDirection = Vector2.zero, OwningPlayer = RopePosition.AssociatedPlayer.One, IsPlayer = true });
        _totalPointCount = 2;

        _lastPlayerOneRopePosition = _ropePositions.Last;
        _lastPlayerTwoRopePosition = _ropePositions.First;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (_ropePositions.Count > 0)
        {
            RopePosition newPlayerOneRopePosition = new RopePosition { Position = PlayerOne.transform.position, ForceDirection = Vector2.zero, IsPlayer = true };
            RopePosition newPlayerTwoRopePosition = new RopePosition { Position = PlayerTwo.transform.position, ForceDirection = Vector2.zero, IsPlayer = true };
            _ropePositions.First.Value = newPlayerOneRopePosition;
            _ropePositions.Last.Value = newPlayerTwoRopePosition;

            float currentRopeDistance = CalculateTotalRopeDistance();
            Debug.Log(currentRopeDistance);
            if (currentRopeDistance > _maxRopeDistance)
            {
                _ropePositions.Clear();
                Debug.Log("YOU DEAD");
            }

            /* Handle rope calculations from player one's side. */
            Vector2 lastRopePoint = _lastPlayerOneRopePosition.Value.Position;
            RaycastHit2D playerToCurrentNextHit = Physics2D.Raycast(PlayerOne.transform.position, (lastRopePoint - (Vector2)PlayerOne.transform.position).normalized, Vector2.Distance(PlayerOne.transform.position, lastRopePoint) - 0.1f, RopeLayerMask);
            //Debug.DrawRay(PlayerOne.transform.position, (lastRopePoint - (Vector2)PlayerOne.transform.position).normalized * (Vector2.Distance(PlayerOne.transform.position, lastRopePoint) - 0.1f), Color.green, 0.1f);

            if (playerToCurrentNextHit && playerToCurrentNextHit.collider != null)
            {
                Vector2 closestPointToHit = GetClosestColliderPointFromRaycastHit(playerToCurrentNextHit, playerToCurrentNextHit.collider);
                LinkedListNode<RopePosition> newNode = _ropePositions.AddBefore(_lastPlayerOneRopePosition, new RopePosition { Position = closestPointToHit, ForceDirection = PlayerOne.PreviousNonZeroVelocity.normalized, OwningPlayer = RopePosition.AssociatedPlayer.One, IsPlayer = false });
                Debug.DrawRay(newNode.Value.Position, newNode.Value.ForceDirection * 10.0f, Color.green, 10.0f);
                ++_totalPointCount;

                _lastPlayerOneRopePosition = _lastPlayerOneRopePosition.Previous;
                if (_lastPlayerTwoRopePosition.Value.IsPlayer)
                    _lastPlayerTwoRopePosition = _lastPlayerOneRopePosition;
            }
            else if (_lastPlayerOneRopePosition.Value.OwningPlayer == RopePosition.AssociatedPlayer.One)
            {
                Vector2 directionFromPlayerOneLastRopePositionToPlayerOne = ((Vector2)PlayerOne.transform.position - _lastPlayerOneRopePosition.Value.Position).normalized;
                //Debug.DrawRay(_lastPlayerOneRopePosition.Value.Position, directionFromPlayerOneLastRopePositionToPlayerOne * 10.0f, Color.green, 0.1f);
                float angleBetweenDirectionAndRopePositionForceDirection = Vector2.Angle(directionFromPlayerOneLastRopePositionToPlayerOne, _lastPlayerOneRopePosition.Value.ForceDirection);
                //Debug.Log("Player One Angle: " + angleBetweenDirectionAndRopePositionForceDirection);
                if (angleBetweenDirectionAndRopePositionForceDirection > 90)
                {
                    LinkedListNode<RopePosition> ropePositionToRemove = _lastPlayerOneRopePosition;
                    _lastPlayerOneRopePosition = _lastPlayerOneRopePosition.Next;
                    _ropePositions.Remove(ropePositionToRemove);

                    if (_ropePositions.Count == 2)
                    {
                        _lastPlayerOneRopePosition = _ropePositions.Last;
                        _lastPlayerTwoRopePosition = _ropePositions.First;
                    }
                    --_totalPointCount;
                }
            }

            /* Handle rope calculations from player two's side. */
            lastRopePoint = _lastPlayerTwoRopePosition.Value.Position;
            playerToCurrentNextHit = Physics2D.Raycast(PlayerTwo.transform.position, (lastRopePoint - (Vector2)PlayerTwo.transform.position).normalized, Vector2.Distance(PlayerTwo.transform.position, lastRopePoint) - 0.1f, RopeLayerMask);
            //Debug.DrawRay(PlayerTwo.transform.position, (lastRopePoint - (Vector2)PlayerTwo.transform.position).normalized * (Vector2.Distance(PlayerTwo.transform.position, lastRopePoint) - 0.1f), Color.red, 0.1f);

            if (playerToCurrentNextHit && playerToCurrentNextHit.collider != null)
            {
                Vector2 closestPointToHit = GetClosestColliderPointFromRaycastHit(playerToCurrentNextHit, playerToCurrentNextHit.collider);
                LinkedListNode<RopePosition> newNode = _ropePositions.AddAfter(_lastPlayerTwoRopePosition, new RopePosition { Position = closestPointToHit, ForceDirection = PlayerTwo.PreviousNonZeroVelocity.normalized, OwningPlayer = RopePosition.AssociatedPlayer.Two, IsPlayer = false });
                Debug.DrawRay(newNode.Value.Position, newNode.Value.ForceDirection * 10.0f, Color.red, 10.0f);
                ++_totalPointCount;

                _lastPlayerTwoRopePosition = _lastPlayerTwoRopePosition.Next;
                if (_lastPlayerOneRopePosition.Value.IsPlayer)
                    _lastPlayerOneRopePosition = _lastPlayerTwoRopePosition;
            }
            else if (_lastPlayerTwoRopePosition.Value.OwningPlayer == RopePosition.AssociatedPlayer.Two)
            {
                Vector2 directionFromPlayerTwoLastRopePositionToPlayerTwo = ((Vector2)PlayerTwo.transform.position - _lastPlayerTwoRopePosition.Value.Position).normalized;
                float angleBetweenDirectionAndRopePositionForceDirection = Vector2.Angle(directionFromPlayerTwoLastRopePositionToPlayerTwo, _lastPlayerTwoRopePosition.Value.ForceDirection);
                //Debug.Log("Player Two Angle: " + angleBetweenDirectionAndRopePositionForceDirection);
                if (angleBetweenDirectionAndRopePositionForceDirection > 90)
                {
                    LinkedListNode<RopePosition> ropePositionToRemove = _lastPlayerTwoRopePosition;
                    _lastPlayerTwoRopePosition = _lastPlayerTwoRopePosition.Previous;
                    _ropePositions.Remove(ropePositionToRemove);

                    if (_ropePositions.Count == 2)
                    {
                        _lastPlayerOneRopePosition = _ropePositions.Last;
                        _lastPlayerTwoRopePosition = _ropePositions.First;
                    }

                    --_totalPointCount;
                }
            }

            RopeRenderer.positionCount = _ropePositions.Count;
            int i = 0;
            foreach (RopePosition ropePosition in _ropePositions.ToList())
            {
                RopeRenderer.SetPosition(i, ropePosition.Position);
                i++;
            }
        }

        //Debug.Log(_ropePositions.Count);
    }

    private Vector2 GetClosestColliderPointFromRaycastHit(RaycastHit2D hit, Collider2D collider)
    {
        Dictionary<float, Vector2> distanceDictionary = new Dictionary<float, Vector2>();
        if (collider is BoxCollider2D)
        {
            BoxCollider2D boxCollider = collider as BoxCollider2D;

            distanceDictionary.Add(Vector2.Distance(hit.point, boxCollider.bounds.max), boxCollider.bounds.max);
            distanceDictionary.Add(Vector2.Distance(hit.point, boxCollider.bounds.min), boxCollider.bounds.min);
            distanceDictionary.Add(Vector2.Distance(hit.point, new Vector2(boxCollider.bounds.max.x, boxCollider.bounds.min.y)), new Vector2(boxCollider.bounds.max.x, boxCollider.bounds.min.y));
            distanceDictionary.Add(Vector2.Distance(hit.point, new Vector2(boxCollider.bounds.min.x, boxCollider.bounds.max.y)), new Vector2(boxCollider.bounds.min.x, boxCollider.bounds.max.y));
        }
        var orderedDictionary = distanceDictionary.OrderBy(e => e.Key);
        return orderedDictionary.Any() ? orderedDictionary.First().Value : Vector2.zero;
    }

    private float CalculateTotalRopeDistance()
    {
        float totalDistance = 0;
        RopePosition previousRopePosition = _ropePositions.First.Value;
        int i = 0;

        foreach (RopePosition ropePosition in _ropePositions.ToList())
        {
            if (i == 0)
            {
                previousRopePosition = ropePosition;
            }
            else
            {
                totalDistance += Vector2.Distance(previousRopePosition.Position, ropePosition.Position);
                previousRopePosition = ropePosition;
            }
            ++i;
        }

        return totalDistance;
    }
}

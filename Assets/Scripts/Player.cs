﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using static StaticTools;

public class Player : MonoBehaviour
{
    [Range(1, 2)]
    public int PlayerID;
    public float Speed;
    public Sprite IdleForward;
    public Sprite IdleBackward;
    public Sprite IdleSide;

    public Vector2 PreviousNonZeroVelocity { get { return _previousNonZeroVelocity; } }


    private Rewired.Player _inputPlayer;
    private Vector2 _input = Vector2.zero;
    private Vector2 _velocity = Vector2.zero;
    private Vector2 _previousNonZeroVelocity = Vector2.zero;
    private Vector2 _previousPosition;
    private SpriteRenderer _renderer;
    private Animator _anim;
    private string _facingDirection = "";
    private Rigidbody2D _rigidBody;


    // Start is called before the first frame update
    void Start()
    {
        _inputPlayer = ReInput.players.GetPlayer(PlayerID - 1);
        _renderer = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _input = _inputPlayer.GetAxis2D(1, 0);

        if (_input.x > 0)
        {
            _renderer.flipX = false;
            _facingDirection = "Right";
        }
        else if (_input.x < 0)
        {
            _renderer.flipX = true;
            _facingDirection = "Left";
        }

        if (_input.y > 0)
            _facingDirection = "Up";
        else if (_input.y < 0)
            _facingDirection = "Down";

        if (_input.x == 0 && _input.y == 0)
        {
            if (_facingDirection == "Right" || _facingDirection == "Left")
                _renderer.sprite = IdleSide;
            else if (_facingDirection == "Down")
                _renderer.sprite = IdleBackward;
            else
                _renderer.sprite = IdleForward;
        }
    }

    private void FixedUpdate()
    {
        _previousPosition = transform.position;

        _velocity = _input * Speed;
        if (!ThresholdApproximately(_velocity.x, 0, 0.01f) || !ThresholdApproximately(_velocity.y, 0, 0.01f))
            _previousNonZeroVelocity = _velocity;

        _anim.SetInteger("Horizontal Velocity", (int)_velocity.x);
        _anim.SetInteger("Vertical Velocity", (int)_velocity.y);

        //transform.Translate(_velocity * Time.fixedDeltaTime);
        _rigidBody.velocity = _velocity;
    }

    public bool HasPlayerMoved()
    {
        return Vector3.Distance(new Vector3(transform.position.x, transform.position.y, 0), _previousPosition) >= Vector3.kEpsilon;
    }
}
